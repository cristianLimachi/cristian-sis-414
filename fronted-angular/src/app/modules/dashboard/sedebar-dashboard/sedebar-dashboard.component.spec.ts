import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SedebarDashboardComponent } from './sedebar-dashboard.component';

describe('SedebarDashboardComponent', () => {
  let component: SedebarDashboardComponent;
  let fixture: ComponentFixture<SedebarDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SedebarDashboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SedebarDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
