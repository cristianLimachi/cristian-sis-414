import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sedebar-dashboard',
  templateUrl: './sedebar-dashboard.component.html',
  styleUrls: ['./sedebar-dashboard.component.css']
})
export class SedebarDashboardComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  public open(){
    document.body.classList.toggle('sb-sidenav-toggled');
  }
}
