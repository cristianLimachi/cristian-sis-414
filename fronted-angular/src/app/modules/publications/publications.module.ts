import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InicioComponent } from './inicio/inicio.component';
import { NoticiasComponent } from './noticias/noticias.component';
import { GaleriaComponent } from './galeria/galeria.component';
import { PublicacionesComponent } from './publicaciones/publicaciones.component';
import { ColeccionComponent } from './coleccion/coleccion.component';
import { RecomendacionesComponent } from './recomendaciones/recomendaciones.component';
import { CatalogoComponent } from './catalogo/catalogo.component';
import { EditInformacionComponent } from './edit-informacion/edit-informacion.component';
import { SharedModule } from '@shared/shared.module';
import { PublicationsRoutingModule } from './publications-routing.module';



@NgModule({
  declarations: [
    InicioComponent,
    NoticiasComponent,
    GaleriaComponent,
    PublicacionesComponent,
    ColeccionComponent,
    RecomendacionesComponent,
    CatalogoComponent,
    EditInformacionComponent
  ],
  imports: [
    SharedModule,
    PublicationsRoutingModule
  ]
})
export class PublicationsModule { }
