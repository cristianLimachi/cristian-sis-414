import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CatalogoComponent } from './catalogo/catalogo.component';
import { ColeccionComponent } from './coleccion/coleccion.component';
import { EditInformacionComponent } from './edit-informacion/edit-informacion.component';
import { GaleriaComponent } from './galeria/galeria.component';
import { InicioComponent } from './inicio/inicio.component';
import { NoticiasComponent } from './noticias/noticias.component';
import { PublicacionesComponent } from './publicaciones/publicaciones.component';
import { RecomendacionesComponent } from './recomendaciones/recomendaciones.component';

const routes: Routes = [
  {
      path:'inicio',
      component:InicioComponent
  },
  {
      path:'noticias',
      component:NoticiasComponent
  },
  {
      path:'galeria',
      component:GaleriaComponent
  },
  {
      path:'publicaciones',
      component:PublicacionesComponent
  },
  {
      path:'coleccion',
      component:ColeccionComponent
  },
  {
      path:'recomendaciones',
      component:RecomendacionesComponent
  },
  {
      path:'catalogo',
      component:CatalogoComponent
  },
  {
      path:'edit_informacion',
      component:EditInformacionComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes,)],
  exports: [RouterModule]
})
export class PublicationsRoutingModule { }
