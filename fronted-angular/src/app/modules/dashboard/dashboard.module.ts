import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeDashboardComponent } from './home-dashboard/home-dashboard.component';
import { NavbarDashboardComponent } from './navbar-dashboard/navbar-dashboard.component';
import { SedebarDashboardComponent } from './sedebar-dashboard/sedebar-dashboard.component';
import { SkeletonDashboardComponent } from './skeleton-dashboard/skeleton-dashboard.component';
import { SharedModule } from '@shared/shared.module';
import { DashboardRoutingModule } from './dashboard-routing.module';



@NgModule({
  declarations: [
    HomeDashboardComponent,
    NavbarDashboardComponent,
    SedebarDashboardComponent,
    SkeletonDashboardComponent
  ],
  imports: [
    SharedModule,
    DashboardRoutingModule
  ]
})
export class DashboardModule { }
